
#include <stdio.h> 
#define M 12
#define N 12

/*
A-->primera matriz 
B-->segunda matriz 
FA-->número de filas de A 
CA-->número de columnas de A 
FB-->número de filas de B 
CB-->número de columnas de B 

C-->matriz resultante de la multiplicación de dimensión FAxCB 
NOTA: 
dos matrices se pueden multiplicar si las columnas de la primera matriz es igual a las filas de la segunda matriz 
*/ 
void multiplicacion(double A[M][N],int FA,int CA,double B[M][N],int FB,int CB,double C[M][N] ){ 
     int i, j, k;  
    if(CA==FB){//para que se puedan multiplicar dos matrices
        for(i=0;i<FA;i++){ 
            for(j=0;j<CB;j++){ 
                C[i][j]=0;//inicializar
                for(k=0;k<CA;k++){ 
                    C[i][j]=C[i][j]+A[i][k]*B[k][j];
                }
            }
        }
    } 
} 

int main (int argc, char *argv[]) { 
    double A[M][N], B[M][N], C[M][N];  
    int i, j, k;  
    
    
    for (int i=0; i<M; i++) {
        for (int j=0; j<N; j++) {
            B[i][j] = C[i][j] = i+j;
        }
    }
    
    multiplicacion(B,M,N,C,M,N,A); 
    
    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            printf("%3.1lf ", A[i][j]);
        }
        printf("\n");
    }
    return 0;
} 