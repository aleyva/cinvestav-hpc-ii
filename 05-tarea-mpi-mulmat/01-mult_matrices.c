//
//  suma_matrices.c
//  
//
//  Created by Amilcar Meneses Viveros on 22/03/19.
//
//

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 16
#define N 16

double a[M][N], b[M][N], c[M][N];

int main(int argc, char **argv){
    int size, rank;
    int i, j, k; //counters
    int l; //size of data sended to each proccess
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank==0) {
        for (i=0; i<M; i++) {
            for (j=0; j<N; j++) {
                b[i][j] = c[i][j] = i+j;
            }
        }
    }
    
    l = N/size;
    MPI_Scatter(b, l*N, MPI_DOUBLE, b, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);    
    MPI_Bcast(&c, M*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    
    if(1){//se supone que el tamaño de las columnas de la matriz b es igual al tamaño de las filas en la matric c
        for(i=0;i<l;i++){
            for(j=0;j<N;j++){
                a[i][j]=0;//inicializar
                for(k=0;k<N;k++)
                    a[i][j]+=b[i][k]*c[k][j];                
            }
        }
    } 


        // printf("rank %d\n", rank);
        // for (i=0; i<M; i++) {
        //     for (j=0; j<N; j++) {
        //         printf("%3.1lf ", a[i][j]);
        //     }
        //     printf("\n");
        // }


    MPI_Gather(a, l*N, MPI_DOUBLE, a, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (rank == 0) {
        for (i=0; i<M; i++) {
            for (j=0; j<N; j++) {
                printf("%3.1lf ", a[i][j]);
            }
            printf("\n");
        }
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}

