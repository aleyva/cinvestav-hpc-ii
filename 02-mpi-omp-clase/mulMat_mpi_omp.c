//
//  mulMat1.c
//  
//
//  Created by Amilcar Meneses Viveros on 22/03/19.
//
//

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 1536
#define N 1536

//double a[M][N], b[M][N], c[M][N];
double *a, *b,*c; 


int main(int argc, char **argv){
    int size, rank;
    int i, j, k, l;
    double t1, t2;
    double tmp;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        l = N/size;

     t1=MPI_Wtime();
     if (rank==0) {
		a= (double *)malloc(M*N*sizeof(double)); 
		b= (double *)malloc(M*N*sizeof(double));
		c= (double *)calloc(M*N,sizeof(double));

		for (i=0; i<M; i++) {
			for (j=0; j<N; j++) {
				b[i*N+j] = i+j;
            }
            c[i*N+i] = 2.0; 
        }
     } else {
        a= (double *)malloc(l*N*sizeof(double));
        b= (double *)malloc(l*N*sizeof(double));
        c= (double *)malloc(M*N*sizeof(double));
    }

    
    MPI_Scatter(b, l*N, MPI_DOUBLE, b, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(c, M*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    #pragma omp parallel for private(j,k,tmp)
    for (i=0; i<l; i++) {
        for (j=0; j<N; j++) {
            tmp = 0.0;
            for (k=0; k<N; k++)
               tmp += b[i*N+k]*c[k*N+j];
            a[i*N+j] = tmp;
        }
    }
    
    MPI_Gather(a, l*N, MPI_DOUBLE, a, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    t2=MPI_Wtime();
    
    if (rank == 0) {
        for (i=M; i<M; i++) {
            for (j=0; j<N; j++) {
                printf("%3.2lf ", a[i*N+j]);
            }
            printf("\n");
        }
        printf("Tiempo %lf\n", t2-t1);
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}

