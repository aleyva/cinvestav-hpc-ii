//
//  mulMat1.c
//  
//
//  Created by Amilcar Meneses Viveros on 22/03/19.
//
//

#include <stdio.h>

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 1536
#define N 1536

double *a, *b, *c;
int size, rank;

__global__ void kernel_mulMat(double *a, double *b, double *c, int l, int m, int n) {
   int i=threadIdx.x+blockDim.x*blockIdx.x; 
   int j=threadIdx.y+blockDim.y*blockIdx.y; 
   int k; 
   double tmp; 
  
   while (i<l) {
      j=threadIdx.y+blockDim.y*blockIdx.y; 
      while(j<n) { 
        tmp=0.0; 
        for (k=0; k<n; k++) {
           tmp+=b[i*n+k]*c[k*n+j];
        }
        a[i*n+j] = tmp;
        j+=blockDim.y*gridDim.y; 
     } 
     i += blockDim.x*gridDim.x; 
   }
}

void mulMat_enDevice(double *a, double *b, double *c, int l, int m, int n) {
   double *aD, *bD, *cD; 
   int sizeab = l*n*sizeof(double); 
   int sizec  = m*n*sizeof(double); 
   dim3 bloques(32,64); 
   dim3 hilos(32,32); 

   cudaSetDevice((2*rank));

   cudaMalloc(&aD, sizeab); cudaMalloc(&bD, sizeab); cudaMalloc(&cD, sizec); 
   
   cudaMemcpy(bD, b, sizeab, cudaMemcpyDefault); 
   cudaMemcpy(cD, c,  sizec, cudaMemcpyDefault);

   kernel_mulMat<<<bloques, hilos>>>(aD, bD, cD, l, m, n); 

   cudaMemcpy(a, aD, sizeab, cudaMemcpyDefault);

   cudaFree(aD); cudaFree(bD); cudaFree(cD); 
  

}   

int main(int argc, char **argv){
//    int size, rank;
    int i, j, k, l;
    int count; 
    double tmp;
    double t1, t2;     

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    l = N/size;
    t1 = MPI_Wtime();

    if (rank==0) {
        a=(double *)malloc(M*N*sizeof(double)); 
        b=(double *)malloc(M*N*sizeof(double));
        c=(double *)calloc(M*N, sizeof(double));

        for (i=0; i<M; i++) {
            for (j=0; j<N; j++) {
                b[i*N+j] = i+j;
            }
            c[i*N+i] = 2.0;
        }
    } else {
        a=(double *)malloc(l*N*sizeof(double));
        b=(double *)malloc(l*N*sizeof(double));
        c=(double *)malloc(M*N*sizeof(double));
    }
    

    MPI_Scatter(b, l*N, MPI_DOUBLE, b, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(c, M*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    cudaGetDeviceCount( &count );
    if (count == 0) {
      printf("Jodido! jodido! jodido!\n"); 
      #pragma omp parallel for private (j, k, tmp)
      for (i=0; i<l; i++) {
        for (j=0; j<N; j++) {
            tmp = 0.0;
            for (k=0; k<N; k++)
               tmp += b[i*N+k]*c[k*N+j];
            a[i*N+j] = tmp;
        }
      }
    } else {
//      printf("Bendito!\n"); 
      mulMat_enDevice(a, b, c, l, M, N); 
    }

    MPI_Gather(a, l*N, MPI_DOUBLE, a, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    t2 = MPI_Wtime(); 

    if (rank == 0) {
        for (i=M; i<M; i++) {
            for (j=0; j<N; j++) {
                printf("%3.2lf ", a[i*N+j]);
            }
            printf("\n");
        }
    }
   
    free(a); free(b); free(c);  
    printf("Tiempo: %lfseg\n", t2-t1); 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}

