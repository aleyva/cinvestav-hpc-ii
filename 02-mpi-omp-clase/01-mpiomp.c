#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <mpi.h>
#include <omp.h>

int main (int argc, char** argv){
    int rank, size; 
    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &size);
    printf("%d, %d\n", rank, size); 
    int nthreads, tid;
    #pragma omp parallel private(tid)
    {      
        tid = omp_get_thread_num();
        nthreads = omp_get_num_threads();
        printf("Soy el hilo OpenMP %d de %d del proceso MPI %d de %d.  Estoy en el nodo %s.\n",  
                tid,nthreads,rank,size,getenv("HOSTNAME")); 
        sleep(5);
    }  
    MPI_Finalize();
    return 0;
}
