// compile
//      gcc -o omp-operations -fopenmp omp-operations.c
// execute
//      export OMP_NUM_THREADS=2
//      ./omp-operations 1000 4

#include <stdio.h>
#include <stdlib.h> //calloc malloc free atoi
#include <omp.h>

#define PRINTD "%5.1lf  "

void add_omp(double *A, double *B, double *res, int SIZE){
	int i,j;
    // matrix Addition.
    #pragma omp parallel for default(none) shared(A,B,res,SIZE) private(i,j)
        for ( i = 0; i < SIZE; i++)
            for ( j = 0; j < SIZE; j++)
                res[i*SIZE+j] = A[i*SIZE+j] + B[i*SIZE+j];
        
}

void sub_omp(double *A, double *B, double *res, int SIZE){
	int i,j;
    // matrix Addition.
    #pragma omp parallel for default(none) shared(A,B,res,SIZE) private(i,j)
        for ( i = 0; i < SIZE; i++)
            for ( j = 0; j < SIZE; j++)
                res[i*SIZE+j] = A[i*SIZE+j] - B[i*SIZE+j];
        
}

void mul_mat_omp(double *A, double *B, double *res, int SIZE) { 
    int i, j, k;
    #pragma omp parallel for default(none) shared(A,B,res,SIZE) private(i,j,k)
    // #pragma omp_set_num_threads(8)
        for ( i = 0; i < SIZE; i++){
            for ( j = 0; j < SIZE; j++){
                res[i*SIZE+j]=0.; // set initial value of resulting matrix C = 0
                for ( k = 0; k < SIZE; k++){
                    res[i*SIZE+j] += A[i*SIZE+k]*B[k*SIZE+j];
                }
                // printf("C: %f \n",C[i][j]);
            }
        }
}

void add(double *A, double *B, double *res, int SIZE){
	int i,j;        
    for ( i = 0; i < SIZE; i++)
        for ( j = 0; j < SIZE; j++)
            res[i*SIZE+j] = A[i*SIZE+j] + B[i*SIZE+j];
        
}

void mul_mat(double *A, double *B, double *res, int SIZE) { 
    int i, j, k; 
    for (i = 0; i < SIZE; i++) { 
        for (j = 0; j < SIZE; j++) { 
            res[i*SIZE+j] = 0; 
            for (k = 0; k < SIZE; k++) {
                res[i*SIZE+j] += A[i*SIZE+k] * B[k*SIZE+j]; 
            }
        } 
    } 
} 

void print_mat(double *A, int SIZE) { 
    int i, j; 
    for (i = 0; i < SIZE; i++) { 
        for (j = 0; j < SIZE; j++) 
            printf(PRINTD, A[i*SIZE+j]);         
        printf("\n"); 
    }   
}

int main(int argc, char* argv[]){
    if(argc != 3){
      fprintf(stderr,"<size> <n_threads> \n",argv[0]);
      return -1;
    }
    int i, j, k;
    int SIZE = atoi(argv[1]);
    int n_threads = atoi(argv[2]);    

    omp_set_num_threads(n_threads);

    double *A = malloc(sizeof(double)*SIZE*SIZE);
    double *B = malloc(sizeof(double)*SIZE*SIZE);
    double *res = calloc(SIZE*SIZE,sizeof(double));

    // Initialize matrices
    for ( i = 0; i < SIZE; i++) {
        for ( j = 0; j < SIZE; j++) {
            A[i*SIZE+j] = (double)i;
            B[i*SIZE+j] = (double)i;
        }
    }
    

    
    // printf("Matriz A:\n");
    // print_mat(A, SIZE);
    // printf("Matriz B:\n");
    // print_mat(B, SIZE);
    
    add_omp (A, B, res, SIZE);
    printf("Matriz suma OMP:\n");
    print_mat(res, SIZE);

    add (A, B, res, SIZE);
    printf("Matriz suma:\n");
    print_mat(res, SIZE);

    // sub_omp (A, B, res, SIZE);
    // printf("Matriz resta:\n");
    // print_mat(res, SIZE);

    mul_mat (A, B, res, SIZE);
    printf("Matriz mul:\n");
    print_mat(res, SIZE);
    
    mul_mat_omp (A, B, res, SIZE);    
    printf("Matriz mul OMP:\n");
    print_mat(res, SIZE);


    // printf( "%s nThrds %d matrix %d maxRT %g minRT %g aveRT %g \
            ave_GFlop/s %g\n ", argv[0], omp_get_num_threads(), SIZE, \
            maxTime, minTime, aveTime, ((2e-9)*SIZE)/aveTime);

   free(A); free(B); free(res);
   return 0;
}



