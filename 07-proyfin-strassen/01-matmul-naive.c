// compile
//      gcc 01-matmul-naive.c -o 01-matmul-naive
// execute
//      ./01-matmul-naive 10

#include <stdio.h> 
#include <stdlib.h>

#define PRINTD "%5.1lf  "
  
void mul_mat(double *A, double *B, double *res, int SIZE) { 
    int i, j, k; 
    for (i = 0; i < SIZE; i++) { 
        for (j = 0; j < SIZE; j++) {              
            for (k = 0; k < SIZE; k++) {
                res[i*SIZE+j] += A[i*SIZE+k] * B[k*SIZE+j]; 
            }
        } 
    } 
} 

void print_mat(double *A, int SIZE) { 
    int i, j; 
    for (i = 0; i < SIZE; i++) { 
        for (j = 0; j < SIZE; j++) 
            printf(PRINTD, A[i*SIZE+j]);         
        printf("\n"); 
    }   
}

int main(int argc, char **argv){     
    if(argc != 2){        
        printf("<row size>\n");	
		exit(EXIT_FAILURE);
    }
    
    int SIZE = atoi(argv[1]);
    double *A, *B, *res;
    int i,j;

    //initialize vectors
    A = (double *)malloc(SIZE*SIZE*sizeof(double));
    B = (double *)malloc(SIZE*SIZE*sizeof(double));
    res = (double *)calloc(SIZE*SIZE,sizeof(double));

	// populate matrix	
    for(i=0;i<SIZE;i++){
        for(j=0;j<SIZE;j++){
            // A[i*SIZE+j] = B[i*SIZE+j] = (double)i*SIZE+j;
            A[i*SIZE+j] = B[i*SIZE+j] = (double)i;
            printf(PRINTD,A[i*SIZE+j]);
        }
        printf("\n");
    }
    
    mul_mat(A, B, res, SIZE); 
  
    printf("Resultado\n"); 
    print_mat(res, SIZE);
    return 0; 
} 
  
