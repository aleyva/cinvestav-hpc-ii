// compile
//      ./MPICC 04-matmul-strassen
// execute
//      ./MPIRUN 4 04-matmul-strassen 10

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define PRINTD "%5.1lf  "

void add_mat_omp(double *A, double *B, double *res, int SIZE){
	int i,j;    
    #pragma omp parallel for default(none) shared(A,B,res,SIZE) private(i,j)
        for ( i = 0; i < SIZE; i++)
            for ( j = 0; j < SIZE; j++)
                res[i*SIZE+j] = A[i*SIZE+j] + B[i*SIZE+j];
        
}

void sub_mat_omp(double *A, double *B, double *res, int SIZE){
	int i,j;    
    #pragma omp parallel for default(none) shared(A,B,res,SIZE) private(i,j)
        for ( i = 0; i < SIZE; i++)
            for ( j = 0; j < SIZE; j++)
                res[i*SIZE+j] = A[i*SIZE+j] - B[i*SIZE+j];
        
}

void mul_mat_omp(double *A, double *B, double *res, int SIZE) { 
    int i, j, k;
    #pragma omp parallel for default(none) shared(A,B,res,SIZE) private(i,j,k)    
        for ( i = 0; i < SIZE; i++){
            for ( j = 0; j < SIZE; j++){
                res[i*SIZE+j]=0;
                for ( k = 0; k < SIZE; k++){
                    res[i*SIZE+j] += A[i*SIZE+k]*B[k*SIZE+j];
                }                
            }
        }
}

void add_mat(double *A, double * B, double *res, int N){				
	int i, j;
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			res[i*N+j] = A[i*N+j] + B[i*N+j];
	
}

void sub_mat(double *A, double * B, double *res, int N){		
	int i, j;
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			res[i*N+j] = A[i*N+j] - B[i*N+j];	
}

void mul_mat(double *A, double *B, double *res, int N) { 
    int i, j, k; 
    for (i = 0; i < N; i++) { 
        for (j = 0; j < N; j++) { 
            res[i*N+j] = 0; 
            for (k = 0; k < N; k++) {
                res[i*N+j] += A[i*N+k] * B[k*N+j]; 
            }
        } 
    } 
} 

void do_submat(double *mat, double *sub_mat, int i_start, int j_start, int N, int MID){
	int i1, i2, j1, j2;
	for (i1 = 0, i2 = i_start; i1 < MID; i1++, i2++)
		for (j1 = 0, j2 = j_start; j1 < MID; j1++, j2++)
			sub_mat[i1*MID+j1] = mat[i2*N+j2];
}

/*Funcion para unir las matrices hijas y matriz padre **/
void join_submat(double *sub_mat, double*mat, int iB, int jB, int MID, int N){
	int i1, i2, j1, j2;
	for (i1 = 0, i2 = iB; i1 < MID; i1++, i2++)
		for (j1 = 0, j2 = jB; j1 < MID; j1++, j2++)
			mat[i2*N+j2] = sub_mat[i1*MID+j1];
}

void print_mat(double *mat, int N) { 
    int i, j; 
    for (i = 0; i < N; i++) { 
        for (j = 0; j < N; j++) 
            printf(PRINTD, mat[i*N+j]);         
        printf("\n"); 
    }   
}


// void mul_mat_rec(double *A, double *B, double *R, int N, int *rank, int *sizeProcesador) {
// 	int MID = N/2;
// 	double *R = (double **)malloc(n*n*sizeof(double));
// 	/* caso base */
// 	if (n == 1) {
// 		R[0*N+0] = A[0*N+0] * B[0*N+0];
// 	}
// 	else {
// 		double *A11 = (double *)malloc(MID*MID*sizeof(double));
// 		double *A12 = (double *)malloc(MID*MID*sizeof(double));
// 		double *A21 = (double *)malloc(MID*MID*sizeof(double));
// 		double *A22 = (double *)malloc(MID*MID*sizeof(double));
// 		double *B11 = (double *)malloc(MID*MID*sizeof(double));
// 		double *B12 = (double *)malloc(MID*MID*sizeof(double));
// 		double *B21 = (double *)malloc(MID*MID*sizeof(double));
// 		double *B22 = (double *)malloc(MID*MID*sizeof(double));

// 		/*Dividing matrix A into 4 halves **/
// 		/*Dividiendo la matriz A en 4 particiones, (infinitamente ... ) */
// 		do_submat(A, A11, 0, 0, MID);
// 		do_submat(A, A12, 0, MID, MID);
// 		do_submat(A, A21, MID, 0, MID);
// 		do_submat(A, A22, MID, MID, MID);
// 		/*Dividing matrix B into 4 halves **/
// 		do_submat(B, B11, 0, 0, MID);
// 		do_submat(B, B12, 0, MID, MID);
// 		do_submat(B, B21, MID, 0, MID);
// 		do_submat(B, B22, MID, MID, MID);

// 		/**
// 		M1 = (A11 + A22)(B11 + B22)
// 		M2 = (A21 + A22) B11
// 		M3 = A11 (B12 - B22)
// 		M4 = A22 (B21 - B11)
// 		M5 = (A11 + A12) B22
// 		M6 = (A21 - A11) (B11 + B12)
// 		M7 = (A12 - A22) (B21 + B22)
// 		**/

// 		double *M1 = mul_mat(add(A11, A22, MID), add(B11, B22, MID), MID, rank, sizeProcesador);
// 		double *M2 = mul_mat(add(A21, A22, MID), B11, MID, rank, sizeProcesador);
// 		double *M3 = mul_mat(A11, sub(B12, B22, MID), MID, rank, sizeProcesador);
// 		double *M4 = mul_mat(A22, sub(B21, B11, MID), MID, rank, sizeProcesador);
// 		double *M5 = mul_mat(add(A11, A12, MID), B22, MID, rank, sizeProcesador);
// 		double *M6 = mul_mat(sub(A21, A11, MID), add(B11, B12, MID), MID, rank, sizeProcesador);
// 		double *M7 = mul_mat(sub(A12, A22, MID), add(B21, B22, MID), MID, rank, sizeProcesador);

// 		/**
// 		C11 = M1 + M4 - M5 + M7
// 		C12 = M3 + M5
// 		C21 = M2 + M4
// 		C22 = M1 - M2 + M3 + M6
// 		**/
// 		double *C11 = add(sub(add(M1, M4, MID), M5, MID), M7, MID);
// 		double *C12 = add(M3, M5, MID);
// 		double *C21 = add(M2, M4, MID);
// 		double *C22 = add(sub(add(M1, M3, MID), M2, MID), M6, MID);

// 		/*join 4 halves into one result matrix **/
// 		juntar(C11, R, 0, 0, MID);
// 		juntar(C12, R, 0, MID, MID);
// 		juntar(C21, R, MID, 0, MID);
// 		juntar(C22, R, MID, MID, MID);
// 	}
// 	return  R;
// }


int main(int argc, char **argv) {
	if(argc != 2){
		printf("<size>\n");
		exit(EXIT_FAILURE);
	}
	
	int rank, size;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); // los ides de cada proceso 
	MPI_Comm_size(MPI_COMM_WORLD, &size); // numero de procesos que lo componen
	
	int i, j;
	long N = atol(argv[1]);	
	int MID = N/2;
	int MID2 = MID*MID;	
	double *M1, *M2, *M3, *M4, *M5, *M6, *M7;

    // int i,j,k,MID;
    // double **a11, **a12, **a21, **a22;
    // double **b11, **b12, **b21, **b22;
    // double *s1, *s2, *s3, *s4, *s5, *s6;
    // double *r1, *r2, *r3, *r4;
    // int *M0, *M1, *M2, *M3, *M4, *M5, *M6;
    // int *c1, *c2, *d1, *d2;
    // int *c11, *c12, *c21, *c22;
	
	double *A11 = (double *)malloc(MID*MID*sizeof(double));
    double *A12 = (double *)malloc(MID*MID*sizeof(double));
    double *A21 = (double *)malloc(MID*MID*sizeof(double));
    double *A22 = (double *)malloc(MID*MID*sizeof(double));
    double *B11 = (double *)malloc(MID*MID*sizeof(double));
    double *B12 = (double *)malloc(MID*MID*sizeof(double));
    double *B21 = (double *)malloc(MID*MID*sizeof(double));
    double *B22 = (double *)malloc(MID*MID*sizeof(double));
    
	if (rank == 0){		
		double *A = (double *)malloc(N*N*sizeof(double));
		double *B = (double *)malloc(N*N*sizeof(double));

		// populate matrix			
		for(i=0; i<N; i++){
			for(j=0; j<N; j++){				
				A[i*N+j] = B[i*N+j] = (double)i;
				// printf(PRINTD,A[i*N+j]);
			}
			// printf("\n");
		}

		// printf("Matriz A\n");
		// print_mat(A, N);		
		

		do_submat(A, A11, 0, 0, N, MID);
        do_submat(A, A12, 0, MID, N, MID);
        do_submat(A, A21, MID, 0, N,  MID);
        do_submat(A, A22, MID, MID, N,  MID);
        
        do_submat(B, B11, 0, 0, N,  MID);
        do_submat(B, B12, 0, MID, N,  MID);
        do_submat(B, B21, MID, 0, N,  MID);
        do_submat(B, B22, MID, MID, N,  MID);		
	}
		
	// send matrix A and B		
    MPI_Bcast(A11, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(A12, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(A21, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(A22, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(B11, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(B12, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(B21, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(B22, MID2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		
/**
// 		M1 = (A11 + A22)(B11 + B22)
// 		M2 = (A21 + A22) B11
// 		M3 = A11 (B12 - B22)
// 		M4 = A22 (B21 - B11)
// 		M5 = (A11 + A12) B22
// 		M6 = (A21 - A11) (B11 + B12)
// 		M7 = (A12 - A22) (B21 + B22)

// 		C11 = M1 + M4 - M5 + M7
// 		C12 = M3 + M5
// 		C21 = M2 + M4
// 		C22 = M1 - M2 + M3 + M6
**/

	if (rank == 0) {		
		double *S1 = (double *)malloc(MID*MID*sizeof(double));
		double *S2 = (double *)malloc(MID*MID*sizeof(double));
		M1 = (double *)malloc(MID*MID*sizeof(double));

		add_mat(A11, A22, S1, MID);
		add_mat(B11, B22, S2, MID);
		
		// printf("Matriz \n");
		// print_mat(S1, MID);

		mul_mat(S1, S2, M1, MID); 		
		
	}
 	if (rank == 1) {		
		double *S3 = (double *)malloc(MID*MID*sizeof(double));
		double *R1 = (double *)malloc(MID*MID*sizeof(double));
		M2 = (double *)malloc(MID*MID*sizeof(double));
		M3 = (double *)malloc(MID*MID*sizeof(double));
		
		add_mat(A21, A22, S3, MID);
		sub_mat(B12, B22, R1, MID);

		mul_mat(S3, B11, M2, MID);		
		mul_mat(A11, R1, M3, MID);

		MPI_Send(M2, MID*MID, MPI_DOUBLE, 0, 200, MPI_COMM_WORLD);
		MPI_Send(M3, MID*MID, MPI_DOUBLE, 0, 300, MPI_COMM_WORLD);

	}
	if (rank == 2) {	
		double *S4 = (double *)malloc(MID*MID*sizeof(double));
		double *R2 = (double *)malloc(MID*MID*sizeof(double));
		M4 = (double *)malloc(MID*MID*sizeof(double));
		M5 = (double *)malloc(MID*MID*sizeof(double));

		sub_mat(B21, B11, R2, MID);
		add_mat(A11, A12, S4, MID);
		
		mul_mat(A22, R2, M4, MID); // M4
		mul_mat(S4, B22, M5, MID); // M5

		MPI_Send(M4, MID*MID, MPI_DOUBLE, 0, 400, MPI_COMM_WORLD);
		MPI_Send(M5, MID*MID, MPI_DOUBLE, 0, 500, MPI_COMM_WORLD);

	}
	
	if (rank == 3) {		
		double *S5 = (double *)malloc(MID*MID*sizeof(double));
		double *S6 = (double *)malloc(MID*MID*sizeof(double));
		double *R3 = (double *)malloc(MID*MID*sizeof(double));
		double *R4 = (double *)malloc(MID*MID*sizeof(double));
		M6 = (double *)malloc(MID*MID*sizeof(double));
		M7 = (double *)malloc(MID*MID*sizeof(double));

		
		add_mat(B11, B12, S5, MID);		
		add_mat(B21, B22, S6, MID);
		sub_mat(A21, A11, R3, MID);
		sub_mat(A12, A22, R4, MID);

		mul_mat(R3, S5, M6, MID); // M6
		mul_mat(R4, S6, M7, MID); // M7

		MPI_Send(M6, MID*MID, MPI_DOUBLE, 0, 600, MPI_COMM_WORLD);
		MPI_Send(M7, MID*MID, MPI_DOUBLE, 0, 700, MPI_COMM_WORLD);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	if (rank==0){	
		
		double *M2 = (double *)malloc(MID*MID*sizeof(double));
		double *M3 = (double *)malloc(MID*MID*sizeof(double));
		double *M4 = (double *)malloc(MID*MID*sizeof(double));
		double *M5 = (double *)malloc(MID*MID*sizeof(double));		
		double *M6 = (double *)malloc(MID*MID*sizeof(double));
		double *M7 = (double *)malloc(MID*MID*sizeof(double));
		double *temp1 = (double *)malloc(MID*MID*sizeof(double));
		double *temp2 = (double *)malloc(MID*MID*sizeof(double));
		double *C11 = (double *)malloc(MID*MID*sizeof(double));
		double *C12 = (double *)malloc(MID*MID*sizeof(double));
		double *C21 = (double *)malloc(MID*MID*sizeof(double));
		double *C22 = (double *)malloc(MID*MID*sizeof(double));

		MPI_Recv(M2, MID*MID, MPI_DOUBLE, 1, 200, MPI_COMM_WORLD, &status);
		MPI_Recv(M3, MID*MID, MPI_DOUBLE, 1, 300, MPI_COMM_WORLD, &status);
		MPI_Recv(M4, MID*MID, MPI_DOUBLE, 2, 400, MPI_COMM_WORLD, &status);
		MPI_Recv(M5, MID*MID, MPI_DOUBLE, 2, 500, MPI_COMM_WORLD, &status);
		MPI_Recv(M6, MID*MID, MPI_DOUBLE, 3, 600, MPI_COMM_WORLD, &status);
		MPI_Recv(M7, MID*MID, MPI_DOUBLE, 3, 700, MPI_COMM_WORLD, &status);

		add_mat(M1, M4, temp1, MID);
		sub_mat(temp1, M5, temp2, MID);
		add_mat(temp2, M7, C11, MID);

		add_mat(M3, M5, C12, MID);
		
		add_mat(M2, M4, C21, MID);
		
		sub_mat(M1, M2, temp1, MID);
		add_mat(temp1, M3, temp2, MID);
		add_mat(temp2, M6, C22, MID);		
		
		double *res = (double *)malloc(N*N*sizeof(double));
		/*join 4 halves into one result matrix **/
		join_submat(C11, res, 0, 0, MID, N);
		join_submat(C12, res, 0, MID, MID, N);
		join_submat(C21, res, MID, 0, MID, N);
		join_submat(C22, res, MID, MID, MID, N);

		printf(" La matriz resultante es : \n");
		print_mat(res, N);
	}

	// free(A); free(B); 
	MPI_Finalize();

	
	return 0;
}