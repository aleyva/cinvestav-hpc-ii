#include "MessengerC.h"
#include "MessengerHandler.h"

#include "ace/OS_NS_stdio.h"
#include "ace/OS_NS_unistd.h"
#include "ace/OS_NS_sys_time.h"
#include <iostream>
#include "ace/Get_Opt.h"        //parser de argumentos
#include "orbsvcs/CosNamingC.h" //namingservice




int ACE_TMAIN (int argc, ACE_TCHAR *argv [])
{
  try {

    // Initialize the ORB
    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
    
    // create and get a reference to Naming Service
    CORBA::Object_var naming_obj = orb->resolve_initial_references("NameService");
    CosNaming::NamingContextExt_var root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
    if (CORBA::is_nil(root.in())) {
      std::cerr << "Nil Naming Context reference" << std::endl;
      return 1;
    }


//**Parte para levantar el servidor del cliente
    // Get reference to Root POA.
    CORBA::Object_var obj = orb->resolve_initial_references( "RootPOA" );
    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj.in());

    // Activate POA manager
    PortableServer::POAManager_var mgr = poa->the_POAManager();
    mgr->activate();

    



    const ACE_TCHAR *server_name = server_name = ACE_TEXT( "UNO" );    
    // Bind the example Naming Context, if necessary
    CosNaming::Name name;
    name.length( 1 );
    name[0].id = CORBA::string_dup( "example" );
    try {
      CORBA::Object_var dummy = root->resolve( name );
    }
    catch(const CosNaming::NamingContext::NotFound&) {
      CosNaming::NamingContext_var dummy = root->bind_new_context( name );
    }    
    name.length( 2 );    
    //name[1].id = CORBA::string_dup( ACE_TEXT( alphabet[i] ) );
    name[1].id = CORBA::string_dup( server_name );

    // Create an object
    PortableServer::Servant_var<MessengerHandler> servant = new MessengerHandler(orb.in());
    PortableServer::ObjectId_var oid = poa->activate_object(servant.in());
    obj = poa->id_to_reference(oid.in());
    root->rebind(name, obj.in());
    std::cout << "Messenger object bound in Naming Service with NAME: " << server_name << std::endl;

    // Register an AMI handler for the Messenger interface    
    AMI_MessengerHandler_var handler = AMI_MessengerHandler::_narrow(obj.in());

    



//**Fin de la parte que funciona como servidor


    CORBA::Object_var messenger_obj = CORBA::Object::_nil();
    CORBA::String_var message_to = CORBA::string_alloc(81);        
    CORBA::String_var nam_context_server = CORBA::string_alloc(81);                
    // while (true) {      
      nam_context_server = CORBA::string_dup("example/");    
      std::cout << "Enter a server -->";
      std::cin.getline(message_to, 81);            
      strcat(nam_context_server, message_to);
      try {
        std::cout << "Buscando: " << nam_context_server << std::endl;
        messenger_obj = root->resolve_str(nam_context_server);
        //obj = root->resolve_str( orb_id );
       
        // Narrow the Messenger object reference.
        Messenger_var messenger = Messenger::_narrow(messenger_obj.in());
        if (CORBA::is_nil(messenger.in())) {
          std::cerr << "Argument is not a Messenger reference" << std::endl;
          return 1;
        }


    // //** Parte que envía el mensaje

        // Record the time the request was made.
         ACE_Time_Value time_sent = ACE_OS::gettimeofday();
        // Send a message the the Messenger object.
        messenger->sendc_send_message(handler.in(), "TAO User", "TAO Test", "Have a nice day");

        // Do some work to prove that we can send the message asynchronously    
        for (int i = 0; i < 10; ++i) {
          ACE_OS::printf(".");
          ACE_OS::sleep(ACE_Time_Value(0, 10 * 1000));
        }

        // Our simple servant will exit as soon as it receives the results.
        orb->run();
        std::cerr << "Argument is not a Messenger reference" << std::endl;
        if (servant->message_was_sent())
        {
          // Note : We can't use the
          ACE_Time_Value delay = ACE_OS::gettimeofday() - time_sent;
          std::cout << std::endl << "Reply Delay = " << delay.msec() << "ms" << std::endl;
        }

        orb->destroy();

      } catch (const CosNaming::NamingContext::NotFound&) {
        // Sleep for a second and try again
        std::cout << "Server not found" << std::endl;
        //ACE_OS::sleep(1);
      }
    // }

    
  }
  catch(const CORBA::Exception& ex) {
    std::cerr << "Caught a CORBA::Exception: " << ex << std::endl;
    return 1;
  }

  return 0;
}
