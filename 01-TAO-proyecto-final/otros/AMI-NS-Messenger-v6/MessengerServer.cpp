#include "Messenger_i.h"
#include <fstream>
#include <iostream>
#include "ace/Get_Opt.h"
#include "orbsvcs/CosNamingC.h"


unsigned int seconds_to_wait = 0;
CORBA::Boolean servant_throws_exception = false;
const ACE_TCHAR *server_name;


int parse_args (int argc, ACE_TCHAR *argv[])
{
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("i:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c)
      {
      case 'i':
        server_name = get_opts.opt_arg();
        break;      
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           "usage:  %s"
                           " -i <orb_id>"                           
                           "\n",
                           argv [0]),
                          -1);
      }

  // Indicates successful parsing of the command line
  return 0;
}


// char naming_server(CORBA::ORB_var orb){  
//   char alphabet[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//   CORBA::Object_var obj = CORBA::Object::_nil();
//   CORBA::String_var nam_context = CORBA::string_dup("example/");    
//   CORBA::String_var nam_context_server = CORBA::string_alloc(81);                    

//   // Find the Naming Service
//   CORBA::Object_var naming_obj = orb->resolve_initial_references("NameService");
//   CosNaming::NamingContextExt_var root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
//   if (CORBA::is_nil(root.in())) {
//     std::cerr << "Nil Naming Context reference" << std::endl;
//     return (' ');
//   }
  
//   // Finde de messenger obj
//   int i = 0;
//   while (i < 26){  
//     try {
//       nam_context_server = CORBA::string_dup("");    
//       strcat(nam_context_server, nam_context);
//       strcat(nam_context_server, CORBA::string_dup(&(alphabet[i])) );
//       std::cout << "Buscar: " << alphabet[i] << std::endl;
//       obj = root->resolve_str( nam_context_server );
//       i++;
//     } catch (const CosNaming::NamingContext::NotFound&) {
//       std::cout << "No se encontró" << std::endl;    
//       break;
//     }

//   }
//   std::cout << "Devolver" << alphabet[i] << std::endl;  
//   //return ACE_TEXT( strcat("", CORBA::string_dup(&(alphabet[i])) ) );
  
//   return alphabet[i];

// }

const ACE_TCHAR * naming_server(CosNaming::NamingContextExt_var search_root){  
  char *alphabet[] = {"A", "B", "C"};
  CORBA::Object_var obj = CORBA::Object::_nil();
  CORBA::String_var nam_context = CORBA::string_dup("example/");    
  CORBA::String_var nam_context_server = CORBA::string_alloc(81);                    

  // Find the Naming Service
  // int argc
  // ACE_TCHAR *argv [];
  // CORBA::ORB_var orb_temp = CORBA::ORB_init(argc, NULL);
  // CORBA::Object_var naming_obj = orb_temp->resolve_initial_references("NameService");
  //CosNaming::NamingContextExt_var root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
  // if (CORBA::is_nil(root.in())) {
  //   std::cerr << "Nil Naming Context reference" << std::endl;
  //   return ACE_TEXT(" ");
  // }
  
  // Finde de messenger obj
  int i = 0;
  while (i < 26){  
    try {
      nam_context_server = CORBA::string_dup("");    
      strcat(nam_context_server, nam_context);
      strcat(nam_context_server, alphabet[i] );
      std::cout << "Buscar: " << alphabet[i] << std::endl;
      obj = search_root->resolve_str( nam_context_server );
      i++;
    } catch (const CosNaming::NamingContext::NotFound&) {
      std::cout << "No se encontró" << std::endl;    
      break;
    }

  }  
  return ACE_TEXT( alphabet[i] );
}





int ACE_TMAIN (int argc, ACE_TCHAR *argv [])
{
  try {
    // Initialize orb
    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
    if (parse_args (argc, argv) != 0)
      return 1;

    // Get reference to Root POA.
    CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj.in());

    // Activate POA manager
    PortableServer::POAManager_var mgr = poa->the_POAManager();
    mgr->activate();

    // Find the Naming Service
    CORBA::Object_var naming_obj = orb->resolve_initial_references( "NameService" );
    CosNaming::NamingContext_var root = CosNaming::NamingContext::_narrow( naming_obj.in() );
    CosNaming::NamingContextExt_var search_root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
    if (CORBA::is_nil(root.in())) {
      std::cerr << "Nil Naming Context reference" << std::endl;
      return 1;
    }


    char *alphabet[] = {"A", "B", "C", "D", "E"};
    CORBA::Object_var search_obj = CORBA::Object::_nil();     
    CORBA::String_var nam_context_server = CORBA::string_alloc(81);                    
    int i = 0;    
    while(i < 5) {
      std::cout << "Veces: " << i << std::endl;
      try {        
        nam_context_server = CORBA::string_dup("example/");            
        strcat(nam_context_server, alphabet[i] );
        //std::cout << "Buscar: " << nam_context_server << std::endl;                        
        search_obj = search_root->resolve_str( nam_context_server );                                        
        i++;   
      } catch (const CosNaming::NamingContext::NotFound&) {
        //std::cout << "No se encontró" << std::endl;            
        break;      
      }       
    }
    server_name = ACE_TEXT( alphabet[i] );

    // Bind the example Naming Context, if necessary
    CosNaming::Name name;
    name.length( 1 );
    name[0].id = CORBA::string_dup( "example" );
    try {
      CORBA::Object_var dummy = root->resolve( name );
    }
    catch(const CosNaming::NamingContext::NotFound&) {
      CosNaming::NamingContext_var dummy = root->bind_new_context( name );
    }    
    name.length( 2 );    
    //name[1].id = CORBA::string_dup( ACE_TEXT( alphabet[i] ) );
    name[1].id = CORBA::string_dup( server_name );
    
    
    // Create an object
    PortableServer::Servant_var<Messenger_i> servant = new Messenger_i(seconds_to_wait, servant_throws_exception);

    // Write its stringified reference to stdout
    PortableServer::ObjectId_var oid = poa->activate_object(servant.in());
    obj = poa->id_to_reference( oid.in() );
    root->rebind(name, obj.in());
    
    //std::cout << "Messenger object bound in Naming Service with name: " << alphabet[i] << std::endl;
    std::cout << "Messenger object bound in Naming Service with NAME: " << server_name << std::endl;

    // Accept requests
    orb->run();
    orb->destroy();
  }
  catch(const CORBA::Exception& ex) {
    std::cerr << "Caught a CORBA::Exception: " << ex << std::endl;
    return 1;
  }

  return 0;
}
