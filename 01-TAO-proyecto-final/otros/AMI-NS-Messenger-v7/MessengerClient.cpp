#include "MessengerC.h"
#include "MessengerHandler.h"

#include "ace/OS_NS_stdio.h"
#include "ace/OS_NS_unistd.h"
#include "ace/OS_NS_sys_time.h"
#include <iostream>
#include "ace/Get_Opt.h"        //parser de argumentos
#include "orbsvcs/CosNamingC.h" //namingservice


const ACE_TCHAR *client_name = ACE_TEXT( "null" );
ACE_TCHAR dependent_set[20];

int parse_args (int argc, ACE_TCHAR *argv[]){
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("n:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c){
      case 'n': client_name = get_opts.opt_arg();      
        break;      
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           "usage:  %s"
                           " -n <server_name>"                           
                           "\n",
                           argv [0]),
                          -1);
      }  
  return 0;
}

int ACE_TMAIN (int argc, ACE_TCHAR *argv [])
{
  try {

    //initializa the default values
    dependent_set[0] = '\0';
    
    // Initialize the ORB
    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
    if (parse_args (argc, argv) != 0 || strcmp(client_name, "null") == 0){
      std::cout << "Es necesario el argumento -n" << std::endl; 
      return 1;
    }

    // create and get a reference to Naming Service
    CORBA::Object_var naming_obj = orb->resolve_initial_references("NameService");
    CosNaming::NamingContextExt_var root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
    if (CORBA::is_nil(root.in())) {
      std::cerr << "Nil Naming Context reference" << std::endl;
      return 1;
    }


//**Parte para levantar el servidor del cliente
    // Get reference to Root POA.
    CORBA::Object_var obj = orb->resolve_initial_references( "RootPOA" );
    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj.in());

    // Activate POA manager
    PortableServer::POAManager_var mgr = poa->the_POAManager();
    mgr->activate();

    // Register an AMI handler for the Messenger interface
    PortableServer::Servant_var<MessengerHandler> servant = new MessengerHandler(orb.in());
    PortableServer::ObjectId_var oid = poa->activate_object(servant.in());
    obj = poa->id_to_reference(oid.in());
    AMI_MessengerHandler_var handler = AMI_MessengerHandler::_narrow(obj.in());
//**Fin de la parte que funciona como servidor


    CORBA::Object_var messenger_obj = CORBA::Object::_nil();
    CORBA::String_var message_to = CORBA::string_alloc(81);        
    CORBA::String_var nam_context_server = CORBA::string_alloc(81);                   
    while (true) {      
      std::cout << "----------------------------------------------------" << std::endl;
      nam_context_server = CORBA::string_dup("example/");    
      std::cout << "  Enviar peticion a servidor -->";
      std::cin.getline(message_to, 81);            
      strcat(nam_context_server, message_to);
      try {
        // std::cout << "  Enviando a servidor: " << message_to << std::endl;
        //messenger_obj = root->resolve_str(message_to);
        messenger_obj = root->resolve_str(nam_context_server);
        //obj = root->resolve_str( orb_id );
       
        // Narrow the Messenger object reference.
        Messenger_var messenger = Messenger::_narrow(messenger_obj.in());
        if (CORBA::is_nil(messenger.in())) {
          std::cerr << "Argument is not a Messenger reference" << std::endl;
          return 1;
        }

    //** Parte que envía el mensaje

        // Record the time the request was made.
        ACE_Time_Value time_sent = ACE_OS::gettimeofday();
        // Send a message the the Messenger object.
        messenger->sendc_send_message(handler.in(), client_name, "", "Hola");

        // Our simple servant will exit as soon as it receives the results.
        orb->run();


        if (servant->message_was_sent())
        {
          //aqui ejecuta un codigo si se envío el mensaje
        }
        
        

      } catch (const CosNaming::NamingContext::NotFound&) {
        // Sleep for a second and try again
        std::cout << "ERROR: No se encontro el servidor" << std::endl;
        //ACE_OS::sleep(1);
      }
    }

    orb->shutdown();

    
  }
  catch(const CORBA::Exception& ex) {
    std::cerr << "Caught a CORBA::Exception: " << ex << std::endl;
    return 1;
  }

  return 0;
}
