#include "Messenger_i.h"
#include <fstream>
#include <iostream>
#include "ace/Get_Opt.h"
#include "orbsvcs/CosNamingC.h"


unsigned int seconds_to_wait = 0;
CORBA::Boolean servant_throws_exception = false;
const ACE_TCHAR *server_name = ACE_TEXT( "null" );


int parse_args (int argc, ACE_TCHAR *argv[]){
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("n:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c){
      case 'n': server_name = get_opts.opt_arg();      
        break;      
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           "usage:  %s"
                           " -n <server_name>"                           
                           "\n",
                           argv [0]),
                          -1);
      }  
  return 0;
}

int ACE_TMAIN (int argc, ACE_TCHAR *argv [])
{
  try {
    // Initialize orb
    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
    if (parse_args (argc, argv) != 0 || strcmp(server_name, "null") == 0){
      std::cout << "Es necesario el argumento -n" << std::endl; 
      return 1;
    }
    
    // Get reference to Root POA.
    CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj.in());

    // Activate POA manager
    PortableServer::POAManager_var mgr = poa->the_POAManager();
    mgr->activate();

    // Find the Naming Service
    CORBA::Object_var naming_obj = orb->resolve_initial_references( "NameService" );
    CosNaming::NamingContext_var root = CosNaming::NamingContext::_narrow( naming_obj.in() );
    CosNaming::NamingContextExt_var search_root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
    if (CORBA::is_nil(root.in())) {
      std::cerr << "Nil Naming Context reference" << std::endl;
      return 1;
    }

    
    // Bind the example Naming Context, if necessary
    CosNaming::Name name;
    name.length( 1 );
    name[0].id = CORBA::string_dup( "example" );
    try {
      CORBA::Object_var dummy = root->resolve( name );
    }
    catch(const CosNaming::NamingContext::NotFound&) {
      CosNaming::NamingContext_var dummy = root->bind_new_context( name );
    }    
    name.length( 2 );    
    //name[1].id = CORBA::string_dup( ACE_TEXT( alphabet[i] ) );
    name[1].id = CORBA::string_dup( server_name );
    
    
    // Create an object
    PortableServer::Servant_var<Messenger_i> servant = new Messenger_i(seconds_to_wait, servant_throws_exception);

    // Write its stringified reference to stdout
    PortableServer::ObjectId_var oid = poa->activate_object(servant.in());
    obj = poa->id_to_reference( oid.in() );
    root->rebind(name, obj.in());
    
    //std::cout << "Messenger object bound in Naming Service with name: " << alphabet[i] << std::endl;
    std::cout << "SERVER NAME: " << server_name << std::endl;
    std::cout << "----------------------------------------------------" << std::endl;

    // Accept requests
    orb->run();
    orb->destroy();
  }
  catch(const CORBA::Exception& ex) {
    std::cerr << "Caught a CORBA::Exception: " << ex << std::endl;
    return 1;
  }

  return 0;
}
