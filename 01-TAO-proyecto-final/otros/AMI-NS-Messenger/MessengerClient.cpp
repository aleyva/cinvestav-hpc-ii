#include "MessengerC.h"
#include "MessengerHandler.h"

#include "ace/OS_NS_stdio.h"
#include "ace/OS_NS_unistd.h"
#include "ace/OS_NS_sys_time.h"
#include <iostream>
#include "ace/Get_Opt.h"        //parser de argumentos
#include "orbsvcs/CosNamingC.h" //namingservice


int parse_args (int argc, ACE_TCHAR *argv[]){
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("k:a:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c)
      {      
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           "usage:  %s"
                           " -k <ior>"
                           " -a <automated>"
                           "\n",
                           argv [0]),
                          -1);
      }
  // Indicates successful parsing of the command line
  return 0;
}

int ACE_TMAIN (int argc, ACE_TCHAR *argv [])
{
  try {

    // Initialize the ORB
    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
    // parse de argumentos
    if (parse_args (argc, argv) != 0)
      return 1;

    // create and get a reference to Naming Service
    CORBA::Object_var naming_obj = orb->resolve_initial_references("NameService");
    CosNaming::NamingContextExt_var root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
    if (CORBA::is_nil(root.in())) {
      std::cerr << "Nil Naming Context reference" << std::endl;
      return 1;
    }

    // Find the server in name service
    CORBA::Object_var obj_client = CORBA::Object::_nil();
    while (CORBA::is_nil(obj_client.in())) {
      try {
        //std::cout << "orb_id: " << orb_id << std::endl;
        obj_client = root->resolve_str("example/A");
      } catch (const CosNaming::NamingContext::NotFound&) {
        std::cout << "No se encontró" << std::endl;    
        // Sleep for a second and try again
        ACE_OS::sleep(1);
      }
    }

    // Narrow the Messenger object reference.
    Messenger_var messenger = Messenger::_narrow(obj_client.in());
    if (CORBA::is_nil(messenger.in())) {
      std::cerr << "Argument is not a Messenger reference" << std::endl;
      return 1;
    }

//**Parte para levantar el servidor del cliente
    // Get reference to Root POA.
    CORBA::Object_var obj = orb->resolve_initial_references( "RootPOA" );
    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj.in());

    // Activate POA manager
    PortableServer::POAManager_var mgr = poa->the_POAManager();
    mgr->activate();

    // Register an AMI handler for the Messenger interface
    PortableServer::Servant_var<MessengerHandler> servant = new MessengerHandler(orb.in());
    PortableServer::ObjectId_var oid = poa->activate_object(servant.in());
    obj = poa->id_to_reference(oid.in());
    AMI_MessengerHandler_var handler = AMI_MessengerHandler::_narrow(obj.in());
//**Fin de la parte que funciona como servidor





//** Parte que envía el mensaje


    // Record the time the request was made.
    ACE_Time_Value time_sent = ACE_OS::gettimeofday();
    // Send a message the the Messenger object.
    messenger->sendc_send_message(handler.in(), "TAO User", "TAO Test", "Have a nice day");

    // Do some work to prove that we can send the message asynchronously    
    for (int i = 0; i < 10; ++i) {
      ACE_OS::printf(".");
      ACE_OS::sleep(ACE_Time_Value(0, 10 * 1000));
    }

    // Our simple servant will exit as soon as it receives the results.
    orb->run();

    if (servant->message_was_sent())
    {
      // Note : We can't use the
      ACE_Time_Value delay = ACE_OS::gettimeofday() - time_sent;
      std::cout << std::endl << "Reply Delay = " << delay.msec() << "ms" << std::endl;
    }

    orb->destroy();
  }
  catch(const CORBA::Exception& ex) {
    std::cerr << "Caught a CORBA::Exception: " << ex << std::endl;
    return 1;
  }

  return 0;
}
