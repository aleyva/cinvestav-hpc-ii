#include "MessengerC.h"

#include "orbsvcs/CosNamingC.h"
#include "ace/OS_NS_unistd.h"
#include "ace/Get_Opt.h"
#include <iostream>
#include <string.h>
#include <sstream>
#define NUMPROC 20
#define WAITINGSEC 1

const ACE_TCHAR *client_name = ACE_TEXT( "null" );

char *depend_set[NUMPROC];   //dependent_set
int total_depend_set;
char *query_set[NUMPROC];   
int total_query_set;
int response_dependent_set;
int blocked;

int parse_args (int argc, ACE_TCHAR *argv[])
{
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("n:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c)
      {
      case 'n':
        client_name = get_opts.opt_arg();
        break;      
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           "usage:  %s"
                           " -n <client_name>"                           
                           "\n",
                           argv [0]),
                          -1);
      }

  // Indicates successful parsing of the command line
  return 0;
}

// int process_response (CORBA::String_var message, CORBA::String_var response_server_name){
//   if(strcmp(message, "BUSY") == 0){
//     std::cout << "  Respondio servidor " << response_server_name << std::endl;    
//     blocked = 1;
//     depend_set[total_depend_set] = (char *)malloc(strlen(response_server_name) + 1);
//     strcpy(depend_set[total_depend_set], response_server_name);
     
//     total_depend_set++;

//     std::cout << "  En 5 segundos iniciar algoritmo para detectar INTERBLOQUEO" << std::endl << "  ";
//     std::cout << ".........." << std::endl;
//     ACE_OS::sleep(5);
//     return 1;    
//   }
//   if(strcmp(message, "SAME_SERVER") == 0){
//     // Aqui se procesa la respuesta cuando se envía al mismo servidor.
//   }  
//   return 0;
// }

int send_message(CosNaming::NamingContextExt_var *root, CORBA::String_var message_to, CORBA::String_var sender_name, CORBA::String_var *message){
  //Declaracion de variables
  CORBA::Object_var messenger_obj = CORBA::Object::_nil();        
  CORBA::String_var nam_context_server = CORBA::string_alloc(81);      
  Messenger_var messenger;         

  //avisa a su sevidor que está bloqueado
  nam_context_server = CORBA::string_dup("example/");    
  strcat(nam_context_server, &message_to[0u]);
  try {
    // std::cout << "  Enviando a servidor: " << message_to << std::endl;        
    messenger_obj = (*root)->resolve_str(nam_context_server);

    // Narrow the Messenger object reference
    messenger = Messenger::_narrow(messenger_obj.in()); 
    if (CORBA::is_nil(messenger.in())) {
      std::cerr << "Not a Messenger reference" << std::endl;
      return 1;
    }

  } catch (const CosNaming::NamingContext::NotFound&) {        
    //std::cout << "ERROR: No se encontro el servidor" << std::endl;
    std::cerr << "ERROR AL ENVIAR EL MENSAJE " << message << "al servidor " << message_to <<  std::endl;
    return 1;
    
  }  
  // if( strcmp(sender_name, "") == 0 ){
  //   messenger->send_message( client_name , "", (*message).inout()); 
  // }else{
  //   messenger->send_message( client_name , sender_name, (*message).inout()); 
  // }  
  messenger->send_message( client_name , sender_name, (*message).inout()); 
  return 0;
}


int consult_recived_messages(){
  return 0;
}

int ACE_TMAIN (int argc, ACE_TCHAR *argv[])
{
  
  //Declaracion de variables
  CosNaming::NamingContextExt_var root;  
  CORBA::ORB_var orb;  

  //inicializar variables 
  total_depend_set = 0;    
  blocked = 0;
  
  try {
    // Initialize orb
    orb = CORBA::ORB_init( argc, argv );
    if (parse_args (argc, argv) != 0 || strcmp(client_name, "null") == 0){
      std::cout << "Es necesario el argumento -n" << std::endl; 
      return 1;
    }

    // Find the Naming Service
    CORBA::Object_var naming_obj = orb->resolve_initial_references("NameService");
    root = CosNaming::NamingContextExt::_narrow(naming_obj.in());
    if (CORBA::is_nil(root.in())) {
      std::cerr << "Nil Naming Context reference" << std::endl;
      return 1;
    }    
  } catch(const CORBA::Exception& ex) {
    std::cerr << "Caught a CORBA exception: " << ex << std::endl;
    return 1;
  }

  std::cout << std::endl << "CLIENT NAME: " << client_name << std::endl;

  //Inicio de la aplicacion

  //Declaracion de variables  
  CORBA::String_var empty = CORBA::string_dup("");      
  CORBA::String_var message_to = CORBA::string_alloc(81);          
  CORBA::String_var message;         
  CORBA::String_var sender_char;
  CORBA::String_var dependent_char;
  int init_alg_deadlock = 0;     
  int deadlock = 0;
  int i, j;

  if (1) {      
    std::cout << "----------------------------------------------------" << std::endl;      
    
    std::cout << "  Enviar peticion a servidor --> ";
    std::cin.getline(message_to, 81);   
    message = CORBA::string_dup("REQUEST");  
    if(send_message( &root, message_to, empty, &message ) == 1) return 1;
              
    // init_alg_deadlock = process_response(message, message_to);    
    if(strcmp(message, "BUSY") == 0){
      std::cout << "  El servidor " << message_to << " se encuentra ocupado" << std::endl;    
      blocked = 1;
      depend_set[total_depend_set] = (char *)malloc(strlen(message_to) + 1);
      strcpy(depend_set[total_depend_set], message_to);      
      total_depend_set++;

      //se puede hacer mas eficiente obteniendo los tiempos del sistema e iniciando el algoritmo de interbloqueo solo cuando ha pasado determinada cantidad de tiempo,
      //todo esto en lugar de usar sleep()
      std::cout << "  En 5 segundos iniciar algoritmo para detectar INTERBLOQUEO" << std::endl;
      std::cout << "    .........." << std::endl;
      ACE_OS::sleep(5);
      init_alg_deadlock = 1;
    }

    if (init_alg_deadlock){
      // ***** inicia algorimto para detectar interbloqueo.            
      //            
      // 1. Le avisa a su propio servidor que está bloqueado
      message = CORBA::string_dup("BLOCKED");
      if(send_message( &root, client_name, empty, &message ) == 1) return 1;      
      ACE_OS::sleep(WAITINGSEC);

      // 2. Envía un query a los servidores en su conjunto dependiente
      std::cout << "    Enviando mensajes al conjunto dependiente" << std::endl;                
      message = CORBA::string_dup("QUERY");      
      for(i=0; i<total_depend_set; i++){            
        message_to = CORBA::string_dup(""); 
        strcat(message_to, &depend_set[i][0u]);
        std::cout << "    Enviando query a servidor: " << message_to << std::endl;                
        if(send_message( &root, message_to, client_name, &message ) == 1) return 1;    
        ACE_OS::sleep(WAITINGSEC);       
      }             

      while(deadlock == 0){        
        
        // 3. Recuperar la información de los querys que le han llegado al servidor
        message = CORBA::string_dup("SYNC_DATA");
        if(send_message( &root, client_name, empty, &message ) == 1) return 1;
        ACE_OS::sleep(WAITINGSEC);       
        
        std::cout << "    MESSAGE: " << message << std::endl;                
        //procesar la informacion recibida del servidor.      
        std::string s(message);      
        size_t begin = 0, end = 0;
        //Dentro de mensaje esta toda la informacion de los querys que le han enviado, esta está separada por el caracter ':'
                
        while ((end = s.find(":", begin)) != std::string::npos) {  
                   
          // middle = s.find(",", begin);
          // std::string sender = s.substr(begin, middle-begin);
          // std::string dependent = s.substr(middle, end - middle);          
          std::string sender = s.substr (begin, end - begin);
          sender_char = CORBA::string_dup(""); 
          strcpy(sender_char, &sender[0u]);          
          begin = end + 1;   
          std::cout << "    SENDERCHAR: " << sender_char << std::endl;    
          // std::cout << "    sender: " << sender << std::endl;
          // std::cout << "    sender_char: " << sender_char << " client_name: " << client_name << std::endl;   
          // verifica si el query le pertenece a este cliente o a otro
          if( strcmp(sender_char, client_name) == 0){            
            
            response_dependent_set++;            
            if(total_depend_set == response_dependent_set){
              std::cout << "  INTERBLOQUEO DETECTADO !!! " <<  std::endl; 
              deadlock = 1;
            }
            
          }else{
            // 	3.1. El cliente retransmite los querys recuperados del servidor.
            // 2. Envía un query a los servidores en su conjunto dependiente
            message = CORBA::string_dup("QUERY");      
            for(j=0; j<total_depend_set; j++){            
              // dependent_char = CORBA::string_dup(""); 
              // strcat(dependent_char, &dependent[0u]);  
              message_to = CORBA::string_dup(""); 
              strcpy(message_to, &depend_set[j][0u]);   
              std::cout << "    Retransmitiendo a servidor: " << message_to << " query: " << sender_char << std::endl;                                        
              if(send_message( &root, message_to, sender_char, &message ) == 1) return 1;
              ACE_OS::sleep(WAITINGSEC);       
              std::cout << "    .........." << std::endl;
            }
            ACE_OS::sleep(WAITINGSEC*2); 
          }
        }
        
        if(deadlock != 1)
          //esperar un tiempo a que se propaguen nuevamente los mensajes
          ACE_OS::sleep(3);
        
      }


      //Initiate a diffusion computation for a blocked process Pi:
      //   send query(i, i, j) to all processes Pj in the dependent set DSi of Pi;
      //   numi(i) := |DSi|; 
      //   waiti(i) := true;
      // //When a blocked process Pk receives a query(i,j, k):
      //   if this is the engaging query for process P, then send query(i, k, m) to all Pm in its dependent set DSk; 
      //     numk(i) :=	|DSi|;
      //     waitk(i) := true
      //   else 
      //     if waitk(i) then 
      //       send a reply(i, k, j) to Pj.
      // When a process Pk receives a reply(i,j, k):
      //   if waitk(i) then
      //     numk(i) := numk(i) — 1; 
      //     if numk(i) = 0 then
      //       if i = k 
      //         then declare a deadlock 
      //       else 
      //         send reply(i, k, m) to the process Pm which sent the engaging query.
      // ***** fin de algoritmo de interbloqueo.            
    }

  }

  orb->destroy();

  return 0;
}
