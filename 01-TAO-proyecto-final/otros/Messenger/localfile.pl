
eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
    & eval 'exec perl -S $0 $argv:q'
    if 0;

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::TestTarget;

$status = 0;
$debug_level = '0';

foreach $i (@ARGV) {
    if ($i eq '-debug') {
        $debug_level = '10';
    }
}

my $ns = PerlACE::TestTarget::create_target (1) || die "Create target 1 failed\n";
my $srv = PerlACE::TestTarget::create_target (2) || die "Create target 2 failed\n";
my $cli = PerlACE::TestTarget::create_target (3) || die "Create target 3 failed\n";

my $nsiorfile = "ns.ior";

my $ns_nsiorfile = $ns->LocalFile ($nsiorfile);
my $srv_nsiorfile = $srv->LocalFile ($nsiorfile);
my $cli_nsiorfile = $cli->LocalFile ($nsiorfile);
$ns->DeleteFile ($nsiorfile);
$srv->DeleteFile ($nsiorfile);
$cli->DeleteFile ($nsiorfile);

# start Naming Service
$NameService = "$ENV{TAO_ROOT}/orbsvcs/Naming_Service/tao_cosnaming";
# $NS = $ns->CreateProcess ($NameService, " -o $ns_nsiorfile");
# $SRV = $srv->CreateProcess ("MessengerServer", "-ORBdebuglevel $debug_level ".
#                                                "-ORBInitRef NameService=file://$srv_nsiorfile");
# $CLI = $cli->CreateProcess ("MessengerClient", "-ORBInitRef NameService=file://$cli_nsiorfile");

print STDERR "TAO_COSNAMING $NameService -o $ns_nsiorfile\n";
print STDERR "SERVER: MessengerServer -ORBdebuglevel $debug_level -ORBInitRef NameService=file://$srv_nsiorfile\n";
print STDERR "CLIENT: MessengerClient -ORBInitRef NameService=file://$cli_nsiorfile\n";
    