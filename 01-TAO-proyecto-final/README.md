

# Template TAO con AMI y Naming Service (NS)


## Compilacion 

### Step 1 (Optional)

Primero hay que creal los archivos con el idl (framework). 
Ya que este es un ejemplo, no es necesario hacer este paso por que ha fue realizado y los archivos se sobreescriben.

```shell
$ tao_idl -GC -GIh _i.h -GIs _i.cpp Messenger.idl
```

### Step 2

Generar archivos makefile

```shell
$ mwc.pl -type gnuace 
```

### Step 3

Compilar

```shell
$ make
```

## Ejecución


### Step 1 NAMING SERVICE: 

Se deje ejecutar en una terminal el servicio de Naming Service

```shell
$ ./tao_cosnaming -o ns.ior
```

Para que esto funcione, es necesario tener el archivo tao_cosnaming que fué compilado en tu computadora, este se encuentra en  
$TAO_ROOT/orbsvcs/Naming_Service/tao_cosnaming

### Step 2 SERVER: 

En otra terminal se debe levantar el servant
En este ejemplo se pueden ejecutar hasta 5 servants, que tendrán por nombre {A, B, C, D y E} 
Nota: el nombramiento de los servants se hace automático
Nota: se debe abrir una terminal por servant

```shell
./MessengerServer -ORBdebuglevel 0 -ORBInitRef NameService=file://ns.ior
```

### Step 3 CLIENT:

En otra terminal se debe levantar el cliente.
No hay un numero maximo de clientes a ejecutar.
Nota: se debe abrir una terminarl por cliente

```shell
./MessengerClient -ORBInitRef NameService=file://ns.ior
```


