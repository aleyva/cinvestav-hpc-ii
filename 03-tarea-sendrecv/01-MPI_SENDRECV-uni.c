#include "mpi.h"
#include <stdio.h>

int main(int argc, char **argv){    
    
    int size, rank, sendbuf, recbuf;    
    int prev, next;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    next = (rank+1)%size;    
    prev = (rank-1 == -1) ? size-1 : rank-1;
    sendbuf = rank*10;
    
    MPI_Sendrecv(&sendbuf, 1, MPI_INT, next, 0, &recbuf, 1, MPI_INT, prev, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
    printf("Hi! I'm the process %d de %d (send %d to %d, recive %d from %d)\n", rank, size, sendbuf, next, recbuf, prev);
    
    
    MPI_Finalize();
    return 0;
}
