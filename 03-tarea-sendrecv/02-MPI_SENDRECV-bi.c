#include "mpi.h"
#include <stdio.h>

int main(int argc, char **argv){    
    
    int size, rank, sendbuf_next, recbuf_next, sendbuf_prev, recbuf_prev;    
    int prev, next;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    next = (rank+1)%size;    
    prev = (rank-1 == -1) ? size-1 : rank-1;
    sendbuf_next = rank*10;
    sendbuf_prev = rank*3;
    
    MPI_Sendrecv(&sendbuf_next, 1, MPI_INT, next, 0, &recbuf_prev, 1, MPI_INT, prev, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&sendbuf_prev, 1, MPI_INT, prev, 0, &recbuf_next, 1, MPI_INT, next, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
    printf("Hi! I'm the process %d de %d\n", rank, size);
    printf("\tsend %d to %d and send %d to %d\n", sendbuf_next, next, sendbuf_prev, prev);
    printf("\trecive %d from %d and recive %d from %d)\n", recbuf_next, next, recbuf_prev, prev);
    
    
    MPI_Finalize();
    return 0;
}
