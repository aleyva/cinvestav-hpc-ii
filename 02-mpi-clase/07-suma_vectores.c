//
//  suma_vectores.c
//  
//
//  Created by Amilcar Meneses Viveros on 22/03/19.
//
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1000

double a[N], b[N], c[N];

int main(int argc, char **argv){
    int size, rank;
    int i, l;
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank==0) {
        for (i=0; i<N; i++)
            b[i] = c[i] = i;
    }

    l = N/size;
    MPI_Scatter(b, l, MPI_DOUBLE, b, l, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(c, l, MPI_DOUBLE, c, l, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    for (i=0; i<l; i++) {
        a[i] = b[i]+c[i];
    }
    
    MPI_Gather(a, l, MPI_DOUBLE, a, l, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (rank == 0) {
        for (i=0; i<N; i++)
            printf("%d -> %lf\n", i, a[i]);
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
