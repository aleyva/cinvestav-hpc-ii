//
//  holaMundo.c
//  
//
//  Created by Amilcar Meneses Viveros on 12/03/19.
//
//

#include "mpi.h"
#include <stdio.h>

int main(int argc, char **argv){

    int size, rank, ss;
    char hostname[MPI_MAX_PROCESSOR_NAME];
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    MPI_Get_processor_name(hostname, &ss);
    
    printf("Hola! soy el proceso %d de %d, en %s\n",
           rank, size, hostname);
    
    
    MPI_Finalize();
    return 0;
}
