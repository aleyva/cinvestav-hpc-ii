//
//  estado.c
//  
//
//  Created by Amilcar Meneses Viveros on 15/03/19.
//
//

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 30

int main(int argc, char **argv){
    int size, rank;
    int numbers[MAX];
    int num_count;
    int i;
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size !=2 ) {
        fprintf(stderr, "Deben ser dos procesos!!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank==0) {
        srand(time(NULL));
        num_count = (rand()/(float)RAND_MAX)*MAX;
        for (i=0; i<num_count; i++) {
            numbers[i] = num_count;
        }
        MPI_Send(numbers, num_count, MPI_INT, 1, 10, MPI_COMM_WORLD);
        printf("Proceso 0 manda %d numeros a proceso 1\n", num_count);
    } else {
        MPI_Recv(numbers, MAX, MPI_INT, 0, 10, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &num_count);
        printf("Proceso 1 recibe %d datos del proceso 0\n", num_count);
        for (i=0; i<num_count; i++) {
            printf("%d) %d\n", i, numbers[i]);
        }
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
