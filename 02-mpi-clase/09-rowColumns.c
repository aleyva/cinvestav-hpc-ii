//
//  rowColumns.c
//  
//
//  Created by Amilcar Meneses on 11/06/15.
//
//

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define M 4
#define N 4

float a[M][N];
float col[M][2];


int main(int argc, char **argv) {
    int i, j;
    int rank, size;
    int nr;
   
    MPI_Datatype column_tA, coluA;
    MPI_Datatype column_tB, coluB;
    
    MPI_Init(&argc, &argv);    
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // if (size != M) {
    //     printf("Error en el numero de procesos MPI, debe ser %d\n",M);
    //     MPI_Finalize();
    //     return -1;
    // }
    
    if (!rank) {
        printf("Matriz original\n");
        int num = 0;
        for ( i=0; i<M; i++) {
            for ( j=0; j<N; j++) {
                a[i][j] = num++;
                printf("%7.2lf ", a[i][j]);
            }
            printf("\n");
        }
        printf("=============\n\n"); 
    }
    
    
    // Manda columna    
    //int MPI_Type_vector(int count,int blocklength,int stride, MPI_Datatype oldtype,MPI_Datatype *newtype)
    MPI_Type_vector(M,1,N,MPI_FLOAT,&coluA);   //Creates a vector (strided) datatype    
    MPI_Type_commit(&coluA);                    //Commits the datatype
    //int MPI_Type_create_resized(MPI_Datatype oldtype,MPI_Aint lb,MPI_Aint extent,MPI_Datatype *newtype)
    MPI_Type_create_resized(coluA, 0, 1*sizeof(float), &column_tA);   //Create a datatype with a new lower bound and extent from an existing datatype    
    MPI_Type_commit(&column_tA);
    
    MPI_Type_vector(M,1,1,MPI_FLOAT,&coluB);
    MPI_Type_commit(&coluB);
    MPI_Type_create_resized(coluB, 0, 1*sizeof(float), &column_tB);
    MPI_Type_commit(&column_tB);
    
    //int MPI_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)       
    MPI_Scatter(a, 1, column_tA, col, 1, column_tB, 0, MPI_COMM_WORLD);
    
   
    MPI_Barrier(MPI_COMM_WORLD);

    for ( i=0; i<M; i++) {
        printf("rank: %d    ", rank);
        for ( j=0; j<2; j++)
            printf("a[%d][%d]=%5.2f ", i, j, col[i][j]);            
        printf("\n", rank);    
    }
    printf("\n", rank);
        
    
    
    MPI_Finalize();
    return 0;
}
