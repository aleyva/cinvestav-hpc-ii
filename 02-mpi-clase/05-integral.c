//
//  integral.c
//  
//
//  Created by Amilcar Meneses Viveros on 15/03/19.
//
//

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define XMIN -10 
#define XMAX 10 
#define NP 10000

double cte(double x) {
    return 1.0/(x+20.0);
}

double integral(double (*f)(double), double xmin, double xmax) {
    int i;
    double x, tmp=0;
    double dx = (xmax-xmin)/((double) (NP));
    
    x = xmin;
    for (i=0; i<NP; i++) {
        tmp += f(x)*dx;
        x+=dx;
    }
    return tmp;
}

int main(int argc, char **argv){
    int size, rank;
    double xmin, xmax, lx;
    double rp, res;
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    lx = (XMAX-XMIN)/size;
    xmin = XMIN+rank*lx;
    xmax = xmin+lx;
    
    rp = integral(cte, xmin, xmax);
    printf("Proceso %d, resultado parcial %lf\n", rank, rp);
    
    MPI_Reduce(&rp, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if (rank==0)
        printf("El resultado total es: %lf\n", res);
    
    MPI_Finalize();
    return 0;
}
