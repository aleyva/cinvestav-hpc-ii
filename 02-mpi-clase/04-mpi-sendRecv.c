//
//  mpi_sendRecv.c
//  
//
//  Created by Amilcar Meneses Viveros on 12/03/19.
//
//

#include "mpi.h"
#include <stdio.h>

int main(int argc, char **argv){
    
    int size, rank, s0=10, s1=30, ss;
    char hostname[MPI_MAX_PROCESSOR_NAME];
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank==0) {
        MPI_Send(&s0, 1, MPI_INT, 1, 10, MPI_COMM_WORLD);
        MPI_Recv(&ss, 1, MPI_INT, 1, 20, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    } else {
        MPI_Recv(&ss, 1, MPI_INT, 0, 10, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(&s1, 1, MPI_INT, 0, 20, MPI_COMM_WORLD);
        
    }
    
    printf("Hola! soy el proceso %d de %d,  (%d, %d, %d)\n",
           rank, size, s0, s1, ss);
    
    
    MPI_Finalize();
    return 0;
}
