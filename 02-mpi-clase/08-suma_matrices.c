//
//  suma_matrices.c
//  
//
//  Created by Amilcar Meneses Viveros on 22/03/19.
//
//

#include <stdio.h>

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 500
#define N 500

double a[M][N], b[M][N], c[M][N];

int main(int argc, char **argv){
    int size, rank;
    int i, j, l;
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank==0) {
        for (i=0; i<M; i++) {
            for (j=0; j<N; j++) {
                b[i][j] = c[i][j] = i+j;
            }
        }
    }
    
    l = N/size;
    MPI_Scatter(b, l*N, MPI_DOUBLE, b, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(c, l*N, MPI_DOUBLE, c, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    for (i=0; i<l; i++) {
        for (j=0; j<N; j++) {
            a[i][j] = b[i][j]+c[i][j];
        }
    }
    
    MPI_Gather(a, l*N, MPI_DOUBLE, a, l*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (rank == 0) {
        for (i=0; i<M; i++) {
            for (j=0; j<N; j++) {
                printf("%3.3lf ", a[i][j]);
            }
            printf("\n");
        }
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}

