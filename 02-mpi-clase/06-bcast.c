//
//  bcast.c
//  
//
//  Created by Amilcar Meneses Viveros on 22/03/19.
//
//

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 30
#define M  4
#define N  4

int a[M][N];

int main(int argc, char **argv){
    int size, rank;
    int i, j;
    
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        for (i=0; i<M; i++) {
            for (j=0; j<N; j++)
                a[i][j] = i;
        }
    }
    MPI_Bcast(&a, M*N, MPI_INT, 0, MPI_COMM_WORLD);
    
    printf("Proceso %d de %d, a[0][0]=%d, a[0][1]=%d, a[1][0]=%d a[1][1]=%d\n",
           rank, size, a[0][0], a[0][1], a[1][0], a[1][1]);
    
    MPI_Finalize();
    return 0;
}
