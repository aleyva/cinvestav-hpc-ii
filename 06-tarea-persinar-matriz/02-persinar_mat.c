#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define N 8

int main(int argc, char* argv[]){	
	//declare variables
    double *A, *B, *C;
	int rank, size, i, j;
	int vc[4], vd[4];
	int N2 = N/2;
	MPI_Datatype QUADS, QS;
	MPI_Datatype QUADR, QR;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Status status;

    //initialize vectors
    A = (double *)malloc(N*N*sizeof(double));
    B = (double *)malloc(N*N*sizeof(double));
    C = (double *)calloc(N*N,sizeof(double));

	// populate matrix
	if(rank==0){
		for(i=0;i<N;i++){
			for(j=0;j<N;j++){
				A[i*N+j] = B[i*N+j] = (double)i*N+j;
				printf("%5.1lf ",A[i*N+j]);
			}
		printf("\n");
		}
	}    

	MPI_Type_vector(N2,N2,N,MPI_DOUBLE,&QUADS);
	MPI_Type_commit(&QUADS);
	MPI_Type_create_resized(QUADS,0,N2*sizeof(double),&QS);
	MPI_Type_commit(&QS);

	MPI_Type_vector(N2,N2,N2 ,MPI_DOUBLE,&QUADR);
	MPI_Type_commit(&QUADR);
	MPI_Type_create_resized(QUADR, 0,sizeof(double),&QR);
	MPI_Type_commit(&QR);

	vc[0] = vc[1] = vc[2] = vc[3] = 1;
	vd[0] = 0; vd[1] = 1; vd[2] = N; vd[3] = N+1;
	
    //scatter quadrants
	MPI_Scatterv(A,vc,vd,QS,B,1,QR,0,MPI_COMM_WORLD);
	
    if(rank==3){
        printf("\n");
		printf("Este es el proceso %d de %d\n",rank,size);	
		for(i=0;i<N2;i++){
			for(j=0;j<N2;j++){
				printf("%5.1lf ",B[i*N2+j]);
			}
			printf("\n");
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
    free(A); free(B); free(C);
	MPI_Finalize();
	return 0;
}

