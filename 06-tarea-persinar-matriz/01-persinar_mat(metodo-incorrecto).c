#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>

#define M 6
#define N 6

int main(int argc, char** argv) {
    int rank, size;  
    int i, j, num, strip_size, strip_m, strip_n ;
    double *A_rearrange, *A_original;
    double *A_strip;
    MPI_Datatype strip;

    MPI_Init(&argc,&argv) ;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank) ;
    MPI_Comm_size(MPI_COMM_WORLD,&size) ;

    if(size % 2 != 0 ){
        printf("El numero de procesos debe ser congruente modulo 2 \n");
        return 1;
    }

    if(rank == 0) {       
        /* genarate matrix A */        
        A_original = (double *)malloc(M*N*sizeof(double));
        A_rearrange = (double *)malloc(M*N*sizeof(double));
        
        printf("Matriz original\n"); 
        for(i = 0, num = 0; i < M; i++) {
            for(j = 0; j < N; j++, num++) {
                A_original[i*N+j] = num; 
                printf("%3.1lf ", A_original[i*N+j]);               
            }
            printf("\n"); 
        }

        /* calculate the strip size */
        int proc_m = size / 2;
        int proc_n = size / proc_m;     
        strip_size = M*N / size;        
        strip_m = M/proc_m;
        strip_n = N/proc_n;

        // re-arrange data        
        int k, l;               
        printf("\n*****************\n"); 
        
        for(i = 0, num=0 ; i < proc_m; i++){
            for(j = 0; j < proc_n; j++){
                for(k = 0; k < strip_m; k++){
                    for(l = 0; l < strip_n; l++){                        
                        //printf("%d %d - ",  (i*strip_m+k),(j*strip_n+l)  );                        
                        // printf("%d ",  (i*strip_m+k)*N+(j*strip_n+l)  );      
                        A_rearrange[num++] = A_original[(i*strip_m+k)*N+(j*strip_n+l)];
                    }
                    // printf("\n");    
                }        
                //printf("\n");
            }    
        }
        // printf("\n*****************\n"); 
    }

    // /* Broadcasting the row, column size of matrix A as well as strip size and matrix B*/
    MPI_Bcast(&strip_m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&strip_n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&strip_size, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    A_strip = (double *)malloc(strip_size*sizeof(double));    
   
    //int MPI_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
    MPI_Scatter(A_rearrange, strip_size, MPI_DOUBLE, A_strip, strip_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);  

    // imprimir la matriz A
    MPI_Barrier(MPI_COMM_WORLD);
       
    
    for (i=0, num = 0; i<strip_m; i++) {        
        printf("rank %d, fila %d - ", rank, i);
        for (j=0; j<strip_n; j++, num++) {
            printf("%5.1lf ", A_strip[num]);               
        }
        printf("\n");   
    }
    
    if(rank == 0) {
        free(A_original);  
        free(A_rearrange);
    }
    free(A_strip);
    MPI_Finalize();        
    
    
    return 0;
}