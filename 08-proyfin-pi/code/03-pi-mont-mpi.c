// compile
//      ./MPICC 03-pi-mont-mpi
// execute
//      ./MPIRUN 4 03-pi-mont-mpi 10000000


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

#define SEED time(NULL)
#define TIMES 10000

int pi_montecarlo(int events, int rank) {
    srand(SEED+rank);
    int count = 0;
    double x,y;
    for(int i = 0; i < events; i++) {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;        
        if( x*x + y*y <= 1 ) count++;
    }        
    return count;
}

int main(int argc, char **argv){
    if(argc != 2){        
        printf("<events>\n");	
		exit(EXIT_FAILURE);
    }
    
    long events = atoi(argv[1]);    
    int size, rank;    
    long partial_events, total_count = 0;        
    long in = 0;
    double t1, t2, each_time, sum_time;    
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);    
    
    t1=MPI_Wtime();

    partial_events = events/size;
    in = pi_montecarlo(partial_events, rank);
    
    //printf("Process %d, count %d of %d, partial PI %lf\n", rank, in, partial_events, ( (double)in / partial_events ) * 4);    

    MPI_Reduce(&in, &total_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    t2=MPI_Wtime();
	each_time = t2-t1;    
    
    MPI_Reduce(&each_time, &sum_time,1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if (rank==0){      
        double pi = ( (double)total_count / events ) * 4;   
        printf("Points inside: %d of %d\n", total_count, events);
        printf("Approximate PI = %g\n", pi);
        printf("Total time with %d proccess: %lf\n", size, sum_time/size);		
    }        
             
    MPI_Finalize();
    return 0;
}