// compilar 
//      gcc -fopenmp 02-pi-mont-omp.c -o 02-pi-mont-omp.out
// execute
//      ./02-pi-mont-omp.out 10000000 8

#include <stdio.h>
#include <stdlib.h>
#include "ran3.h"
#include <omp.h>
#include <sys/time.h>

#define SEED time(NULL)

int pi_montecarlo(long val,long idum){
	float x,y;
	int i;
	long in = 0;
	struct s_ran3 rand_var;
	rand_var.iff=0;
	for(i=0; i<val;i++)
	{
		x = ran3(&idum, &rand_var);
		y = ran3(&idum, &rand_var);
		//printf("x:%f, y:%f\n", x, y); 		
		if( x*x + y*y <= 1 ) in++;	
			//printf("J1:%ld\n",j1);
	}
	return in;
}

int main(int argc, char *argv[]){
	if(argc != 3){
		printf("<eventos>, <threads>\n");
		exit(EXIT_FAILURE);
	}

	int i;		
	int num_threads = atoi(argv[2]);
	long events = atol(argv[1]);		    			
	long in = 0;
	long *idum = (long *)calloc(num_threads, sizeof(long));
	struct timeval  tv1, tv2;

	gettimeofday(&tv1, NULL);	
	
	for(i = 0; i < num_threads; i++){		
		idum[i] = -getpid();				
		// idum[i] = SEED;
	}
	
	omp_set_num_threads(num_threads);

	#pragma omp parallel for private(i) schedule(dynamic) reduction(+:in)	
		for(i = 0; i < num_threads; i++){
			in += pi_montecarlo(events/num_threads,idum[i]);
			// printf("thread %d in %d of %d\n",i,in,events/num_threads)	;
		}	
	gettimeofday(&tv2, NULL);

	double pi = ( (double)in / events ) * 4; 	
	printf("Points inside: %ld of %ld \n", in, events);
	printf("Approximate Pi = %lf\n", pi);
	printf("Total time with %d threads = %f seconds\n", num_threads,
         (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec));
	
	return 0;
}

