
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

typedef struct s_ran3{
	int inext,inextp;
	long ma[56];
	int iff;
}s_ran3;

float ran3(long *idum, struct s_ran3 *datos){
	
	long mj,mk;
	int i,ii,k;
	if (*idum < 0 || datos->iff == 0)
	{
		datos->iff=1;
		mj=labs(MSEED-labs(*idum));
		mj %= MBIG;
		datos->ma[55]=mj;
		mk=1;
		for (i=1;i<=54;i++){
			ii=(21*i) % 55;
			datos->ma[ii]=mk;
			mk=mj-mk;
			if (mk < MZ) 
				mk += MBIG;
			mj=datos->ma[ii];
		}
		for (k=1;k<=4;k++)
			for (i=1;i<=55;i++){
				datos->ma[i] -= datos->ma[1+(i+30) % 55];
				if (datos->ma[i] < MZ) 
					datos->ma[i] += MBIG;
			}
		datos->inext = 0;
		datos->inextp = 31;
		*idum=1;
	}
	if (++(datos->inext) == 56) datos->inext=1;
	if (++(datos->inextp) == 56) datos->inextp=1;
	mj = datos->ma[datos->inext] - datos->ma[datos->inextp];
	if (mj < MZ) mj += MBIG;
	datos->ma[datos->inext] = mj;
	return mj*FAC;
}
