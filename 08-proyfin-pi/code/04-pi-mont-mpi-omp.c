// compile
//      ./MPICCOMP 04-pi-mont-mpi-omp
// execute
//      ./MPIRUNOMP 4 04-pi-mont-mpi-omp 100000 8

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ran3.h"
#include <omp.h>
#include <mpi.h>

#define SEED time(NULL)
#define TIMES 10000

int pi_montecarlo(long val,long idum){
	float x,y;
	int i;
	long in = 0;
	struct s_ran3 rand_var;
	rand_var.iff=0;
	for(i = 0; i < val; i++){
		x = ran3(&idum, &rand_var);
		y = ran3(&idum, &rand_var);				
		if( x*x + y*y <= 1 ) in++;		
	}
	return in;
}

int main(int argc, char **argv){
    if(argc != 3){
		printf("<eventos>, <threads>\n");
		exit(EXIT_FAILURE);
	}
    
    int size, rank, i;    
    long partial_count, partial_events, total_count = 0;	
	int num_threads = atoi(argv[2]);
	long events = atol(argv[1]);	    	
	double t1, t2, each_time, sum_time;    

    // preparing OMP
	long in = 0;
	long *idum = (long *)calloc(num_threads, sizeof(long));
        
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);    
    
	t1=MPI_Wtime();

	for(i = 0; i < num_threads; i++){		
		// idum[i] = -getpid();				
		idum[i] = SEED+rank;
	}

    partial_events = events/size;    
    
	omp_set_num_threads(num_threads);

    // OMP part
    #pragma omp parallel for private(i) schedule(dynamic) reduction(+:in)		
		for(i = 0; i < num_threads; i++){
			in += pi_montecarlo(partial_events/num_threads,idum[i]);	
		}
    
    // printf("Process %d, count %d of %d, partial PI %lf\n", rank, in, partial_events, ( (double)in / partial_events ) * 4);
    
	MPI_Reduce(&in, &total_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	t2=MPI_Wtime();
	each_time = t2-t1;    
    
    MPI_Reduce(&each_time, &sum_time,1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank==0){
        double pi = ( (double)total_count / events ) * 4; 
        printf("Points inside: %d of %d\n", total_count, events);
        printf("Approximate PI = %g\n", pi);		
		printf("Total time with %d proccess and %d threads each = %f\n", size, num_threads, sum_time/size);	
    }        
    
    MPI_Finalize();
    return 0;
}