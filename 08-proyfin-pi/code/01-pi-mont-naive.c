// compile
//      gcc 01-pi-mont-naive.c -o 01-pi-mont-naive
// execute
//      ./01-pi-mont-naive 100000

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#define SEED time(NULL)

int pi_montecarlo(long events) {
    srand(SEED);
    long count = 0;
    double x,y,z;
    for(int i = 0; i < events; i++) {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;        
        if( x * x + y * y <= 1 ) count++;
    }        
    return count;
}

int main(int argc, char **argv){     
    if(argc != 2){        
        printf("<events>\n");	
		exit(EXIT_FAILURE);
    }
    
    long events = atoi(argv[1]);
    struct timeval  tv1, tv2;
        
    gettimeofday(&tv1, NULL);
    
    long total_count = pi_montecarlo(events);        
    
    gettimeofday(&tv2, NULL);
    
    double pi = ( (double)total_count / events ) * 4; 
       
    printf("Points inside: %d of %d\n", total_count, events);
    printf("Approximate PI = %g\n", pi);    
    printf ("Total time = %f seconds\n",
         (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec));
    
    return 0;
}