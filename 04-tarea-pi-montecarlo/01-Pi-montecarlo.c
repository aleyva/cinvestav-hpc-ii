#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define SEED time(NULL)
#define TIMES 10000

int pi_montecarlo(int times, int rank) {
    srand(SEED+rank);
    int count = 0;
    double x,y,z;
    for(int i = 0; i < times; i++) {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;
        z = x * x + y * y;
        if( z <= 1 ) count++;
    }        
    return count;
}

int main(int argc, char **argv){
    int size, rank;    
    int times, partial_count, total_count;        
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    times = TIMES;
    partial_count = pi_montecarlo(times, rank);
    printf("Process %d, count %d of %d\n", rank, partial_count, times);
    
    MPI_Reduce(&partial_count, &total_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if (rank==0){
        double pi = ( (double)total_count / (times * size) ) * 4; 
        printf("Total count %d of %d\n", total_count, times * size);
        printf("Approximate PI = %g\n", pi);
    }        
    
    MPI_Finalize();
    return 0;
}